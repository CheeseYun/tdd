const utils = require('./utils');
const should = require('should');

describe('util.js 모듈의 capitializse() 함수는', ()=>{
  it('문자열의 첫번째 문자를 대문자로 변경한다.', ()=>{
    const result = utils.capitializse('hello');
    // assert.equal(result,'Hello');
    result.should.be.equal('Hello');
  });
});
